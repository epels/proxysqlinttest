package main_test

import (
	"context"
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"os"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func TestProxySQL(t *testing.T) {
	dsn := dsn(t)
	t.Logf("Connecting to %q", dsn)

	db, err := sql.Open("mysql", dsn)
	panicOnErr(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var count int
	err = db.QueryRowContext(ctx, `SELECT COUNT(*) FROM mysql_servers`).Scan(&count)
	panicOnErr(t, err)
	t.Logf("Got server count: %d", count)
}

func dsn(t *testing.T) string {
	t.Helper()

	return (&mysql.Config{
		Addr:                 must(t, os.Getenv("HOST")) + ":" + must(t, os.Getenv("PORT")),
		Net:                  "tcp",
		User:                 must(t, os.Getenv("USERNAME")),
		Passwd:               must(t, os.Getenv("PASSWORD")),
		AllowNativePasswords: true,
	}).FormatDSN()
}

func must[T comparable](t *testing.T, val T) T {
	t.Helper()

	var z T
	if val == z {
		panic("value is zero")
	}
	return val
}

func panicOnErr(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatalf("Unexpected error: %s", err)
	}
}
